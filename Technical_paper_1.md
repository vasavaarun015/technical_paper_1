# **Awk and Sed Commands**

## **i) Awk Command**

___


` Awk is a scripting language used for manipulating data and generating reports.The awk command programming language requires no compiling, and allows the user to use variables, numeric functions, string functions, and logical operators.`


`Awk is abbreviated from the names of the developers – Aho, Weinberger, and Kernighan. `

---

## **WHAT CAN WE DO WITH AWK ?**

### **1. AWK Operations:**

    *  Scans a file line by line 
    *  Splits each input line into fields 
    *  Compares input line/fields to pattern
    *  Performs action(s) on matched lines 


### **2. Useful For:**

    * Transform data files 
    * Produce formatted reports 
### **3. Programming Constructs:**

    * Format output lines
    * Arithmetic and string operations 
    * Conditionals and loops 

## **Syntax:**
    * awk options 'selection _criteria {action }' input-file > output-file

## **Awk Command Examples:**
1.  **To print the 2nd and 3rd columns, execute the command below.**

    ` $ awk '{print $2 "\t" $3}' file.txt `
  

    ![Output](https://cdn.journaldev.com/wp-content/uploads/2019/01/awk-print-second-anf-third-column-1.png)



1. **Printing all lines in a file**
  
      ` $ awk ' {print $0}' file.txt `

      ![Output](https://cdn.journaldev.com/wp-content/uploads/2019/01/awk-print-all-lines.png)


1. **Printing all lines that match a specific pattern**
  
    >*Syntex:  awk '/variable_to_be_matched/ {print $0}' file.txt `*
    
    
    For instance, to match all entries with the letter ‘o’, the syntax will be

    ` $ awk '/o/ {print $0}' file.txt `

    ![output](https://cdn.journaldev.com/wp-content/uploads/2019/01/awk-matching-lines.png)

1. **Printing columns that match a specific pattern**
  
    ` $ awk '/a/ {print $3 "\t" $4}' file.txt `

    The above command prints the 3rd and 4th columns where the letter ‘a’ appears in either of the columns


    ![output](https://cdn.journaldev.com/wp-content/uploads/2019/01/awk-matching-column-with-a-PNG.png)

1. **Counting and Printing Matched Pattern**
    
    ` $ awk '/a/{++cnt} END {print "Count = ", cnt}' file.txt  `

    ![output](https://cdn.journaldev.com/wp-content/uploads/2019/01/count-columns-matching-a-criteria.png)

1. **Print Lines with More or less than a No. of Characters**

    ` $ awk 'length($0) > 20' file.txt `



    ![output](https://cdn.journaldev.com/wp-content/uploads/2019/01/print-lines-with-more-or-less-characters.png)

1. **Saving output of AWK to a different file**
   
   ` $ awk '/a/ {print $3 "\t" $4}' file.txt > Output.txt `

   ` $ cat output.txt `


   ![output](https://cdn.journaldev.com/wp-content/uploads/2019/01/awk-redirect-output.png)

---
---


## **ii) Sed Command**

---
` SED command in UNIX is stands for stream editor and it can perform lot’s of function on file like, searching, find and replace, insertion or deletion. Though most common use of SED command in UNIX is for substitution or for find and replace. By using SED you can edit files even without opening it, which is much quicker way to find and replace something in file, than first opening that file in VI Editor and then changing it. `


* SED is a powerful text stream editor. Can do insertion, deletion, search and replace(substitution).

## **Syntex:**
        sed OPTIONS... [SCRIPT] [INPUTFILE...] 


## **Sed Command Examples:**

### **i) Insertion**

1. **Insert one blank line after each line**
   
    ` $ sed G a.txt `

    ![output](https://media.geeksforgeeks.org/wp-content/cdn-uploads/sed-output.png)

1. **insert two blank lines**
   
   ` $ sed 'G;G' a.txt `

1. **Delete blank lines and insert one blank line after each line**
   
   ` $ sed '/^$/d;G' a.txt `

1. **Insert a blank line below every line which matches “love”**
   
   ` sed '/love/G' a.txt `


2. **Insert 5 spaces to the left of every lines**
   
   ` $ sed 's/^/  <5-spaces> /' a.txt `

---

### **ii) Numbering lines**

1. **Number each line of a file (left alignment). **=** is used to number the line. \t is used for tab between number and sentence**

    ` sed =  a.txt | sed 'N;s/\n/\t/' `

1. **Number each line of a file (number on left, right-aligned). This command is similar to `cat -n filename`**

    ` sed = a.txt | sed 'N; s/^/     /; s/ *\(.\{4,\}\)\n/\1  /' `

1. **Number each line of file, only if line is not blank**

    ` sed '/./=' a.txt | sed '/./N; s/\n/ /' `

---
### **iii) Deleting lines**

1. **Delete a particular line**

    >*Syntax: sed ‘nd’ filename*

    ` ex. sed '5d' a.txt `

2. **Delete the last line**

    >*Syntax: sed ‘$d’ filename*

    ` ex. sed ‘$d’ filename  `

3. **Delete line from range x to y**


    >*Syntax:sed ‘x,yd’ filename*

    ` ex. sed '3,5d' a.txt `

1. **Delete from nth to last line**

    >*Syntax: sed ‘nth,$d’ filename*
    
    ` sed '2,$d' a.txt `

2. **Delete the patter matching line**
 
    >*sed ‘/pattern/d’ filename*  

    ` ex. sed '/life/d' a.txt `

3. **Delete lines starting from nth line and every 2nd line from there**

    >*Syntax: sed ‘n~2d’ filename*

    ` ex. sed '3~2d' a.txt `

1. **Delete the lines which matches the pattern and 2 lines after to that**

    >*Syntax: sed ‘/pattern/,+2d’ filename*

    ` ex. sed '/easy/,+2d' a.txt `

1. **Delete blank Lines**

    ` sed '/^$/d' a.txt `

1. **Delete empty lines or those begins with “#”**

    ` sed -i '/^#/d;/^$/d' a.txt `

---
### **iv) View/Print the files**

 *if we need to view a particular section in the middle of any file? Here we’ll discuss, how to use SED command to view a section of any file.* 

1. **Viewing a file from x to y range**

    >*Viewing a file from x to y range

    `  ex. sed -n '2,5p' a.txt `

1. **View the entire file except the given range**

    >*Syntax: sed ‘x,yd’ filename*

    ` ex. sed '2,4d' a.txt `

1. **Print nth line of the file**

    >*Syntax: sed -n ‘address’p filename*

    ` ex. sed -n '4'p a.txt `

1. **Print lines from xth line to yth line.**

    >*Syntax: sed -n ‘x,y’p filename*

    ` ex. sed -n '4,6'p a.txt `

1.  **Print only the last line**

    >*Syntax: sed -n ‘$’p filename*

1. **Print from nth line to end of file**

    >*Syntax: sed -n ‘n,$p’ filename*

    ` ex. sed -n '3,$'p a.txt `

### ***Pattern Printing***

7. **Print the line only which matches the pattern**

    >*Syntax: sed -n /pattern/p filename*

    ` ex. sed -n /every/p a.txt `
1. **Print lines which matches the pattern i.e from input to xth line**

    >*Syntax: sed -n ‘/pattern/,xp’ filename*

    ` ex. sed -n '/everyone/,5p' a.txt `

1. **Prints lines from the xth line of the input, up-to the line which matches the pattern. If the pattern doesn’t found then it prints up-to end of the file.**

    >*Syntax: sed -n ‘x,/pattern/p’ filename*

    ` ex. sed -n '1,/everyone/p' a.txt `

1. **Print the lines which matches the pattern up-to the next xth lines**

    >*Syntax: sed -n ‘/pattern/,+xp’ filename*

    ` ex. sed -n '/learn/,+2p' a.txt `

---
### **v) Replacement with the sed command**

1. **Change the first occurrence of the pattern**

    ` ex. sed 's/life/leaves/' a.txt `

1. **Replacing the nth occurrence of a pattern in a line**

    >*Syntax: sed ‘s/old_pattern/new_pattern/n’ filename*

    ` ex. sed 's/to/two/2' a.txt `

1. **Replacing all the occurrence of the pattern in a line.**

    ` ex. sed 's/life/learn/g' a.txt `

1. **Replace pattern from nth occurrence to all occurrences in a line.**

    >*Syntax: sed ‘s/old_pattern/new_pattern/ng’ filename*

    ` ex. sed 's/to/TWO/2g' a.txt `

    *This sed command replaces the second, third, etc occurrences of pattern “to” with “TWO” in a line.*

    If you wish to print only the replaced lines, then use “-n” option along with “/p” print flag to display only the replaced lines 

    ` ex. sed -n 's/to/TWO/p' a.txt `

    *if you wish to print the replaced lines twice, then only use “/p” print flag without “-n” option*

    ` ex. sed 's/to/TWO/p' a.txt `

1. **Replacing pattern on a specific line number. Here, “m” is the line number.**

    >*yntax: sed ‘m s/old_pattern/new_pattern/’ filename*

    ` ex. sed '3 s/every/each/' a.t `

    *If you wish to print only the replaced lines*

    ` ex. sed -n '3 s/every/each/p' a.txt `

1. **sed -n '3 s/every/each/p' a.txt**

    >*Syntax: sed ‘x,y s/old_pattern/new_pattern/’ filename*

    *x = starting line number* 

    *y = ending line number*

    ` ex. sed '2,5 s/to/TWO/' a.txt `

    *can be used in place of “y” if we wish to change the pattern up-to last line in the file*

    ` ex. sed '2,$ s/to/TWO/' a.txt    `



1. **If you wish to replace pattern in order to ignore character case (beginning with uppercase or lowercase), then there are two ways to replace such patterns**

    >*Syntax: sed ‘s/old_pattern/new_pattern/i’ filename*

    ` ex. sed 's/life/Love/i' a.txt `

    By using regular expressions

    ` ex. sed 's/[Ll]ife/Love/g' a.txt `

1. **To replace multiple spaces with a single space**

    ` ex. sed 's/  */ /g' filename `

1. **Replace one pattern followed by the another pattern**

    >*Syntax: sed ‘/followed_pattern/ s/old_pattern/new_pattern/’ filename*

    ` ex. sed '/is/ s/live/love/' a.txt `

1. **Replace a pattern with other except in the nth line.**

    >*Syntax: sed ‘n!s/old_pattern/new_pattern/’ filename*

    ` ex. sed -i '5!s/life/love/' a.txt `

#### **References**:
 * [Awk command](https://www.google.com/search?channel=fs&client=ubuntu&q=use+of+awk+command+in+unix)

* [Sed Command](https://www.google.com/search?q=sed+command+in+unix&client=ubuntu&hs=oBk&channel=fs&sxsrf=ALeKk01J-PKIi_zQNIy5SOqcQxpcbW5YiQ%3A1629691204680&ei=RB0jYe_pKMz0rAGd5pG4Cw&oq=&gs_lcp=Cgdnd3Mtd2l6EAEYAjIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJzIHCCMQ6gIQJ0oECEEYAFAAWABgq-wFaAFwAngAgAHcAYgB3AGSAQMyLTGYAQCgAQGwAQrAAQE&sclient=gws-wiz)
---

# **Thank You..**



    











   













